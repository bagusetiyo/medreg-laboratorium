# Changelog MedReg Laboratorium

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [5.2.0.10] - 2018-07-12
## Menu Data Pemeriksaan
- **Changed :**
	1. Optimasi menu.
- **Fixed :**
	1. Fixed kategori RADIOLOGI tidak muncul pada pilihan kategori.
	2. Fixed membatasi jumlah pembagian fee jasa petugas medis yang tidak sesuai.

## Menu Data Dokter
- **Added :**
	1. Tambah pilihan DOKTER, PERAWAT atau PEGAWAI.
	2. Tambah input jam praktek petugas medis.

- **Changed :**
	1. Ganti judul menjadi Data Petugas Medis.

- **Removed :**
	1. Hapus input persentase fee jasa dokter.

## Menu Laporan Jadwal Petugas Medis
- **Added :**
	1. Menu baru untuk melihat jadwal petugas medis.

## Menu Import Data Petugas Medis
- **Changed :**
	1. Optimasi menu.
	2. Ubah format template excel.

## Menu Data Pemeriksaan Laboratorium
- **Added :**
	1. Tambah input persentase pembagian fee jasa petugas medis.

- **Changed :**
	1. Optimasi menu.

## Menu Laporan Fee Jasa Petugas Medis
- **Added :**
	1. Tambah input Asisten Dokter, Perawat 1 dan Perawat 2.

- **Changed :**
	1. Tambah informasi No. RM dan Nama Pasien.
	2. Tambah pilihan RADIOLOGI pada kategori pencarian.

# [5.2.0.9] - 2018-03-15
## Menu Pencarian Data Pasien
- **Changed :**
	1. Data yang tampil urut berdasarkan nomor RM.

# [5.2.0.8] - 2018-03-14
## Menu Master Pasien
- **Added :**
	1. Tambah tombol hapus untuk data pasien yang belum pernah reservasi.
	2. Cetak Kartu menggunakan format file kartu.fr3.

- **Changed :**
	1. Tampilan jumlah pasien menggunakan format thousand separator.

- **Fixed :**
	1. Fix Auto Generate ID Pasien tidak sesuai.

# [5.2.0.7] - 2017-11-17
## Menu Import Data Pasien
- **Changed :**
	1. Optimasi menu.

- **Fixed :**
	1. Fix kategori tidak otomatis masuk jika belum ada.
	2. Fix error ketika tanggal registrasi dan tanggal lahir tidak diisi.